require("@nomicfoundation/hardhat-toolbox");
require('dotenv').config();

const { RPC_URL, PRIVATE_KEY } = process.env;
/** @type import('hardhat/config').HardhatUserConfig */
module.exports = {
  solidity: "0.8.17",
  defaultNetwork: "base",
  networks: {
    hardhat: {},
    base: {
      url: RPC_URL,
      accounts: [`0x${PRIVATE_KEY}`]
    }
  },
};
