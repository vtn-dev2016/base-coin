# Example of issuing coins on base.org

This project for test only.

Here's how to deploy it:
1. add coinbase faucet to your deployer address
How to add faucet to your deployer address following document https://docs.base.org/tools/network-faucets
2. create .env on root project then add keys to .env file following this:
```shell
RPC_URL="https://goerli.base.org"
PRIVATE_KEY="YOUR_PRIVATEKEY"
```

Once you are done with the above, run the following command to deploy your contract.
```shell
npm i
npx hardhat compile
npx hardhat run scripts/deploy.js
```
