// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.9;
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

// Uncomment this line to use console.log
// import "hardhat/console.sol";
contract BASE is ERC20 {
    constructor() ERC20("Fixed", "FIX") {
        //10M BASE 
        _mint(msg.sender, 10000000e18);
    }
}
